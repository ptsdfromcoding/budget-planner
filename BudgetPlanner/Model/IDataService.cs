﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace BudgetPlanner.Model
{
    public interface IDataService
    {
        void GetProjects(Action<ObservableCollection<ProjectData>, Exception> callback);
        void AddProject(ProjectData pd);
        void AddProjectsList(ObservableCollection<ProjectData> pdset);
        void UpdateProject(ProjectData pd);
        void UpdateAllProjects(ObservableCollection<ProjectData> pdset);
        void GetBudget(Action<Budget> callback);
        void SetBudget(double budget);
        void GetDatabaseName(Action<String> callback);
        void SetDatabaseName(String db);
    }
}
