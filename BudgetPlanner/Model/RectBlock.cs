﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudgetPlanner.Model
{
    public class RectBlock : ObservableObject
    {
        public int Position { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string HexColor { get; set; }
        public string Name { get; set; }

        public RectBlock(double x, double y, double width, double height, string name, string hexcolor)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            HexColor = hexcolor;
            Name = name;
        }
    }
}
