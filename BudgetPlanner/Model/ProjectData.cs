﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;

namespace BudgetPlanner.Model
{
    public class ProjectData : ObservableObject
    {
        public int Id { get; set; }
        public double Cost { get; set; }
        public double BreakEvenTime { get; set; }
        public string Name { get; set; }
        public double Risk { get; set; }
        public int Priority { get; set; }

        public ProjectData(double cost, double betime, string name, double risk, int priority)
        {
            this.Cost = cost;
            this.BreakEvenTime = betime;
            this.Name = name;
            this.Risk = risk;
            this.Priority = priority;
        }

        public ProjectData() { }
    }
}
