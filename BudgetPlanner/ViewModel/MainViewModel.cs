﻿using GalaSoft.MvvmLight;
using BudgetPlanner.Model;
using System.Collections.ObjectModel;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BudgetPlanner.ViewModel
{
    public class MainViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly IDataService _dataService;
        string filefilter = "LiteDB database files | *.db";

        ObservableCollection<ProjectData> _projects;
        ObservableCollection<RectBlock> _rectBlocks;
        ProjectData _curProject;
        //Budget _budget;
        string _dbName;
        Random random;
        Sorter _sorter;
        double _budget;
        double _income;

        double _presenterWidth;
        double _presenterHeight;

        public ObservableCollection<ProjectData> Projects
        {
            get { return _projects; }
            set
            {
                _projects = value;
                RaisePropertyChanged("Projects");
            }
        }
        public ObservableCollection<RectBlock> RectBlocks
        {
            get { return _rectBlocks; }
            set
            {
                _rectBlocks = value;
                RaisePropertyChanged("RectBlocks");
            }
        }
        public ProjectData CurrentProject
        {
            get { return _curProject; }
            set
            {
                _curProject = value;
                RaisePropertyChanged("CurrentProject");
            }
        }
        public double budget
        {
            get { return _budget; }
            set
            {
                _budget = value;
                RaisePropertyChanged("budget");
            }
        }
        public double PresenterHeight { get { return _presenterHeight; } set { _presenterHeight = value; RaisePropertyChanged("PresenterHeight"); } }
        public double PresenterWidth { get { return _presenterWidth; } set { _presenterWidth = value; RaisePropertyChanged("PresenterHeight"); } }
        public double income { get { return _income; } set { _income = value; RaisePropertyChanged("income"); } }

        //бюджет надо будет вынести в отдельный объект класса вместо double
        /*
        public Budget Budg
        {
            get { return _budget; }
            set
            {
                _budget = value;
                RaisePropertyChanged("Budg");
            }
        }*/
        public string DBName
        {
            get { return _dbName; }
            set
            {
                _dbName = value;
                RaisePropertyChanged("DBName");
            }
        }

        /// <summary>
        /// Конструктор вьюмодельки;
        /// Выполняется одноразово локатором при инициализации приложения
        /// </summary>
        /// <param name="dataService"></param>
        public MainViewModel(IDataService dataService)
        {
            //Вытаскивание датасервиса
            _dataService = dataService;

            //Создание объектов под проекты для отображения
            CurrentProject = new ProjectData();
            Projects = new ObservableCollection<ProjectData>();
            RectBlocks = new ObservableCollection<RectBlock>();
            PresenterHeight = 300;
            PresenterWidth = 300;
            budget = 0;
            income = 0;
            //Budg = new Budget() { Money = 0 };

            random = new Random();

            //Одноразовое выполнение вытаскивания проекта и бюджета
            ExecuteGetProjects();
            ExecuteGetBudget();
            ExecuteGetDBName();
            _sorter = new Sorter();

            //Присваивание методов рилэйкомандам
            GetProjects = new RelayCommand(ExecuteGetProjects);
            AddProject = new RelayCommand(ExecuteAddProject);
            AddProjectsList = new RelayCommand(ExecuteAddProjectsList);
            GenerateProjectsList = new RelayCommand(ExecuteGenerateProjectsList);
            UpdateAllProjects = new RelayCommand(ExecuteUpdateAllProjects);
            GetBudget = new RelayCommand(ExecuteGetBudget);
            SetBudget = new RelayCommand(ExecuteSetBudget);
            GetDBName = new RelayCommand(ExecuteGetDBName);
            SetDBName = new RelayCommand(ExecuteSetDBName);
            CreateNewDB = new RelayCommand(ExecuteCreateNewDB);
            SortProjects = new RelayCommand(ExecuteSortProjects);
        }

        //Объявление рилэйкоманд
        public RelayCommand GetProjects
        {
            get;
            private set;
        }
        public RelayCommand AddProject
        {
            get;
            private set;
        }
        public RelayCommand AddProjectsList { get; private set; }
        public RelayCommand GenerateProjectsList { get; private set; }
        public RelayCommand UpdateAllProjects { get; private set; }
        public RelayCommand GetBudget
        {
            get;
            private set;
        }
        public RelayCommand SetBudget
        {
            get;
            private set;
        }
        public RelayCommand GetDBName
        { get; private set; }
        public RelayCommand SetDBName
        { get; private set; }
        public RelayCommand CreateNewDB
        {
            get;
            private set;
        }
        public RelayCommand SortProjects { get; private set; }



        //Имплементация методов для рилэйкоманд
        private void ExecuteGetProjects()
        {
            _dataService.GetProjects(
                (projects, error) =>
                {
                    if (error != null)
                    {
                        MessageBox.Show("It's shit. No projects somehow.");
                        return;
                    }
                    else
                    {
                        Projects = new ObservableCollection<ProjectData>(projects);
                    }
                });
        }
        private void ExecuteAddProject()
        {
            CurrentProject.Id = 0;
            _dataService.AddProject(CurrentProject);
            Projects.Add(_curProject);
            CurrentProject = new ProjectData();
        }
        private void ExecuteAddProjectsList()
        {
            _dataService.AddProjectsList(Projects);
            ExecuteGetProjects();
        }
        private void ExecuteGenerateProjectsList()
        {
            Projects = generateProjectsList(5, random, budget, 0.2, 1.0, 20, 90);
            ExecuteAddProjectsList();
            ExecuteGetProjects();
        }
        private void ExecuteUpdateAllProjects()
        {
            _dataService.UpdateAllProjects(Projects);
            ExecuteGetProjects();
        }
        private void ExecuteGetBudget()
        {
            /*_dataService.GetBudget(
                (budget) =>
                {
                    Budg = budget;
                });*/
        }
        private void ExecuteSetBudget()
        {
            _dataService.SetBudget(budget/*new Budget { Money = Budg.Money }*/);
            RaisePropertyChanged("budget");
            MessageBox.Show("Budget successfully set: " + budget.ToString(), "Success!", MessageBoxButton.OK);
        }
        private void ExecuteGetDBName()
        {
            _dataService.GetDatabaseName(
                (databaseName) =>
                {
                    DBName = databaseName;
                });
        }
        private void ExecuteSetDBName()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = filefilter
            };
            if (openFileDialog.ShowDialog() == true)
            {
                DBName = openFileDialog.FileName;
                _dataService.SetDatabaseName(DBName);
                ExecuteGetProjects();
            }
        }
        private void ExecuteCreateNewDB()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = filefilter
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                DBName = saveFileDialog.FileName;
                _dataService.SetDatabaseName(DBName);
                ExecuteGetProjects();
            }
        }
        private void ExecuteSortProjects()
        {
            try
            {
                _sorter = new Sorter(Projects, budget);
                RectBlocks = _sorter.Rectangle;
                double pw = 0;
                double ph = 0;
                foreach (RectBlock r in RectBlocks)
                {
                    pw = r.Width > pw ? r.Width : pw;
                    ph = r.Height > ph ? r.Height : ph;
                }
                PresenterWidth = pw;
                PresenterHeight = ph;
                income = _sorter.RoundBudget[_sorter.RoundBudget.Count-1];
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(ex.ToString(), "Your database is most likely empty");
            }
        }


        //генерация проектов, надо будет перекинуть в отдельную вьюмодельку
        private ObservableCollection<ProjectData> generateProjectsList(
            int projectsAmount,
            Random rand,
            double startBudget,
            double minCostPercentage,
            double maxCostPercentage,
            double minTime,
            double maxTime
            )
        {
            ObservableCollection<ProjectData> pdset = new ObservableCollection<ProjectData>();
            ProjectData newProject;

            for (int i = 0; i<projectsAmount;i++)
            {
                newProject = new ProjectData {
                    Id = 0,
                    Name = "proj" + i.ToString(),
                    Cost = rand.Next(Convert.ToInt16(startBudget * minCostPercentage), Convert.ToInt16(startBudget * maxCostPercentage)),
                    BreakEvenTime = rand.Next(Convert.ToInt16(minTime), Convert.ToInt16(maxTime)),
                    Risk = rand.Next(0, 100),
                    Priority = rand.Next(0, 5)
                };
                pdset.Add(newProject);
            }

            return pdset;
        }


        //зачем это было??
        /*
        private List<int> GetPositions(double budget, ObservableCollection<ProjectData> pjcol)
        {
            List<int> pos = new List<int>();

            foreach (ProjectData pj in pjcol)
            {

            }

            return pos;
        }
        */

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}