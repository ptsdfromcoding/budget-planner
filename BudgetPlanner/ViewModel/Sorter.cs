﻿using BudgetPlanner.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace BudgetPlanner.ViewModel
{
    public class Sorter
    {
        ObservableCollection<RectBlock> _rectangle;
        List<double> _weight;
        List<ProjectData> _pj;
        double _budget;

        List<double> _roundBudget;

        double _costWeight = 0.20;
        double _beWeight = 0.20;
        double _priorityWeight = 0.20;
        double _riskWeight = 0.40;

        double riskGains = 0.3;

        public ObservableCollection<RectBlock> Rectangle
        {
            get { return _rectangle; }
            private set
            {
                _rectangle = value;
            }
        }

        public List<double> RoundBudget { get => _roundBudget; set => _roundBudget = value; }

        public Sorter()
        {

        }

        public Sorter(ObservableCollection<ProjectData> pj, double budget)
        {
            _weight = CalcWeight(pj);
            _pj = SortByWeight(pj, _weight);
            _budget = budget;
            Rectangle = BestFitDecreasingHeight(_pj, _budget);
        }

        List<double>CalcWeight(ObservableCollection<ProjectData> pjset)
        {
            List<double> w = new List<double>();
            double MaxCost = pjset.Max(x => x.Cost);
            double MaxTime = pjset.Max(x => x.BreakEvenTime);

            foreach(ProjectData pj in pjset)
            {
                w.Add(_costWeight * (pj.Cost/MaxCost) + _beWeight * (1-pj.BreakEvenTime/MaxTime) + _priorityWeight * (pj.Priority / 5) + _riskWeight * (1 - 0.01*pj.Risk));
            }

            return w;
        }

        List<ProjectData> SortByWeight(ObservableCollection<ProjectData> pjset, List<double> weight)
        {
            List<ProjectData> newset = pjset.ToList();

            int i = 0;
            bool t = true;
            double tempweight;
            ProjectData tempproj;

            while (t)
            {
                t = false;
                for (int j = 0; j<=newset.Count-i-2;j++)
                {
                    if (weight[j] < weight[j+1])
                    {
                        tempweight = weight[j + 1];
                        weight[j + 1] = weight[j];
                        weight[j] = tempweight;

                        tempproj = newset[j + 1];
                        newset[j + 1] = newset[j];
                        newset[j] = tempproj;

                        t = true;
                    }
                }
                i++;
            }

            return newset;
        }

        ObservableCollection<RectBlock> BestFitDecreasingHeight(List<ProjectData> pj, double budget)
        {
            ObservableCollection<RectBlock> rb = new ObservableCollection<RectBlock>();
            Random random = new Random();
            String color;

            List<double> moneySpent = new List<double>();
            List<double> roundStart = new List<double>();
            List<double> roundDuration = new List<double>();
            List<int> round = new List<int>();

            //экспериментально
            List<double> roundBudget = new List<double>();
            roundBudget.Add(budget);
            roundBudget.Add(budget);

            bool checker = false;
            int BestRound = 0;
            double BestPlace = 0;  //Y
            roundDuration.Add(pj[0].BreakEvenTime);
            moneySpent.Add(pj[0].Cost);
            round.Add(0);
            random = new Random(GetSeed(pj[0]));
            color = GetColor(random);
            rb.Add(new RectBlock(0, 0, pj[0].BreakEvenTime, pj[0].Cost, pj[0].Name, color));

            //экспериментально
            if (random.NextDouble() > 0.01 * pj[0].Risk)
            {
                roundBudget[1] += riskGains * pj[0].Cost;
            }
            else
            {
                roundBudget[1] -= riskGains * pj[0].Cost;
            }

            for (int i = 1; i<pj.Count; i++)
            {
                checker = false;
                BestPlace = 0;
                BestRound = roundDuration.Count();
                for(int k = 0; k<roundDuration.Count; k++)
                {
                    if(pj[i].Cost<=roundBudget[k]-moneySpent[k] && BestPlace <= moneySpent[k])
                    {
                        BestPlace = moneySpent[k];
                        BestRound = k;
                        checker = true;
                    }
                }
                if(!checker)
                {
                    moneySpent.Add(pj[i].Cost);
                    round.Add(roundDuration.Count);
                    roundDuration.Add(pj[i].BreakEvenTime);
                    random = new Random(GetSeed(pj[i]));
                    color = GetColor(random);
                    rb.Add(new RectBlock(-1, 0, pj[i].BreakEvenTime, pj[i].Cost, pj[i].Name, color));

                    //экспериментально
                    roundBudget.Add(roundBudget.Last());
                    if (random.NextDouble() > 0.01 * pj[i].Risk)
                    {
                        roundBudget[roundBudget.Count-1] += riskGains * pj[i].Cost;
                    }
                    else
                    {
                        roundBudget[roundBudget.Count - 1] -= riskGains * pj[i].Cost;
                    }
                }
                else
                {
                    
                    round.Add(BestRound);
                    random = new Random(GetSeed(pj[i]));
                    color = GetColor(random);
                    rb.Add(new RectBlock(-1, moneySpent[BestRound], pj[i].BreakEvenTime, pj[i].Cost, pj[i].Name, color));

                    //экспериментально
                    if (random.NextDouble() > 0.01 * pj[i].Risk)
                    {
                        for (int k = BestRound + 1; k < roundBudget.Count; k++)
                        {
                            roundBudget[k] += riskGains * pj[i].Cost;
                        }
                    }
                    else
                    {
                        for (int k = BestRound + 1; k < roundBudget.Count; k++)
                        {
                            roundBudget[k] -= riskGains * pj[i].Cost;
                        }
                    }

                    moneySpent[BestRound] += pj[i].Cost;
                    if (pj[i].BreakEvenTime >= roundDuration[BestRound])
                    {
                        roundDuration[BestRound] = pj[i].BreakEvenTime;
                    }
                    
                }
            }
            roundStart.Add(0.0);
            double tempDuration = 0;
            for(int i = 0; i<roundDuration.Count;i++)
            {
                tempDuration += roundDuration[i];
                roundStart.Add(tempDuration);
            }
            for(int i = 0; i<rb.Count;i++)
            {
                rb[i].X = roundStart[round[i]];
                rb[i].Position = round[i];
            }

            RoundBudget = roundBudget;
            return rb;
        }

        String GetColor(Random rand)
        {
            return String.Format("#{0:X6}", rand.Next(0x1000000));
        }

        int GetSeed(ProjectData pj)
        {
            return pj.Name.GetHashCode()+ Convert.ToInt16(pj.BreakEvenTime * pj.Cost)+ Convert.ToInt16(pj.Priority * pj.Risk);
        }
    }
}
