﻿using System;
using System.Collections.ObjectModel;
using BudgetPlanner.Model;
using System.Linq;
using System.Windows;

namespace BudgetPlanner.Design
{
    public class DesignDataService : IDataService
    {
        Budget b;
        double bt;
        String database = "Projects.db";


        public void GetProjects(Action<ObservableCollection<ProjectData>, Exception> callback)
        {
            var projects = new ObservableCollection<ProjectData>();
            try
            {
                using (var db = new LiteDB.LiteDatabase(@database))
                {
                    projects = new ObservableCollection<ProjectData>(db.GetCollection<ProjectData>("projects").FindAll());
                }
            }
            catch (LiteDB.LiteException ex)
            {
                callback(null, ex);
            }
            callback(projects, null);
        }

        public void AddProject(ProjectData pd)
        {
            try
            {
                using (var db = new LiteDB.LiteDatabase(@database))
                {
                    var projects = db.GetCollection<ProjectData>("projects");
                    projects.Insert(pd);
                }
            }
            catch (LiteDB.LiteException ex)
            {
                MessageBox.Show(ex + "\n shit's not working, check yo project");
            }
        }

        public void AddProjectsList(ObservableCollection<ProjectData> pdset)
        {
            try
            {
                using (var db = new LiteDB.LiteDatabase(@database))
                {
                    var projects = db.GetCollection<ProjectData>("projects");
                    projects.Insert(pdset);
                }
            }
            catch (LiteDB.LiteException ex)
            {
                MessageBox.Show(ex + "\n this generation is doomed man");
            }
        }

        public void UpdateProject(ProjectData pd)
        {
            try
            {
                using (var db = new LiteDB.LiteDatabase(@database))
                {
                    var projects = db.GetCollection<ProjectData>("projects");
                    projects.Update(pd);
                }
            }
            catch (LiteDB.LiteException ex)
            {
                MessageBox.Show(ex + " can't update it properly");
            }
        }

        public void UpdateAllProjects(ObservableCollection<ProjectData> pdset)
        {
            try
            {
                using (var db = new LiteDB.LiteDatabase(@database))
                {
                    var projects = db.GetCollection<ProjectData>("projects");
                    projects.Update(pdset);
                }
            }
            catch (LiteDB.LiteException ex)
            {
                MessageBox.Show(ex + " can't update it properly");
            }
        }

        public void GetBudget(Action<Budget> callback)
        {
            callback(b);
        }

        public void SetBudget(double budget)
        {
            bt = budget;
        }

        public void GetDatabaseName(Action<String> callback)
        {
            callback(database);
        }

        public void SetDatabaseName(String db)
        {
            database = db;
        }
    }
}